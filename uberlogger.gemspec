spec = Gem::Specification.new
spec.author = 'Chayim I. Kirshen'
spec.date = Time.now
spec.add_dependency('log4r', '>=1.1.9')
spec.files = Dir['lib/*.rb']
spec.homepage = 'http://www.gnupower.net'
spec.license = 'MIT'
spec.name = 'uberlogger'
spec.version = '0.3'
spec.summary = "Uberlogger is a wrapper around log4r, to make life lazier."
spec.description = <<-EOF
  Uberlogger wraps log4r to make log setups lazy. The goal is to provide a global logger similar to the python logging class. Fetching an instance of the uberlogger effectively creates an object."

  UberLogger.instance.getLogger('logger-name')
EOF
spec.email = 'ckirshen@gnupower.net'
