# Author: Chayim I. Kirshen (chayim@gnupower.net)
# Date: Dec 20, 2011
# License: MIT (http://www.gnupower.net/licence/license.html)

require "singleton"
require "rubygems"
require "log4r"
require "log4r/outputter/syslogoutputter"

class UberLogger
	"""This is a super logger, literally an uber-logger. The goal is to provide
	a python-like logging facility, with relatively simple to use logging functionality
	that supports multiple logging mechanisms.
	
	x = UberLogger.instance.getLogger('logger-name')
	"""
	
  include Singleton
  
  @@_loggers = Hash.new
  
  def getLogger(name)
    """Creates a new logger, and associates it with a name.
    If the logger already exists, it's returned"""

    if not @@_loggers.has_key?(name) then
      @@_loggers[name] = UberLog.new(name)
    end
    
    return @@_loggers[name]
  end
  
end

class UberLog
  """This is the actual Uberlog class. It's really just a wrapper around
  the Log4r, that makes it easy to deal with at a high level.
  The idea is to have a single location for total logging configuration
  """
  include Log4r

  attr_accessor :name, :fmt, :console, :syslog, :file

  MAXBYTES = 1024**3 # 1 MB logs as the default size
  
  def initialize(name)
    @name = name
    #change this because of error in log4r describe here:
    # http://holmwood.id.au/~lindsay/2008/05/01/datamapper-log4r/
    @log = Log4r::Logger.new(name)

    # I like my logs formatted a very specific way. If you don't deal  
    @fmt = PatternFormatter.new(:pattern => "%d - [%l] [%t]: %m")

    # logger types that are initialized
    @console = false
    @syslog = false
    @file = {}
  end
  
  def _set_level(name)
    """Returns the log level, wrapped.
    Users of logging systems are used to the 7 syslog facilities - this handles wrapping
    that within Log4r, and makes things a bit lazier (case insentitive strings!)
    """
    if (name.downcase == "debug") then
      return Log4r::DEBUG
    elsif (name.downcase == "info") then
      return Log4r::INFO
    elsif (name.downcase == "fatal") then
      return Log4r::FATAL
    elsif (name.downcase == "warn") then
      return Log4r::WARN
    elsif (name.downcase == "error") then
      return Log4r::ERROR
    elsif (name.downcase =="emerg" or name.downcase == "alert" or name.downcase == "crit") then
      return Log4r::FATAL
    else
      raise "Invalid log level specified"
    end
  end
  
  def add_file(filename, maxbytes=MAXBYTES, level=nil)
    """Adding a file logger, that rotates"""
    if @file.key?(filename)
      return
    end

    filelogger = Log4r::RollingFileOutputter.new(@name, 
      {"filename" => filename,
        "maxsize" => maxbytes, 
        "trunc" => false,
      })
      filelogger.formatter = @fmt
    if level
      filelogger.level = _set_level(level)
    end
    @log.add(filelogger)

    @file[filename] = filename

  end
  
  def add_syslog(level=nil)
    """Ensure this logger, logs to syslog as well"""
    if @syslog
      return
    end

    syslogger = Log4r::SyslogOutputter.new(@name)
    syslogger.formatter = @fmt
    if level
      syslogger.level = _set_level(level)
    end    
    @log.add(syslogger)

    @syslog = true
  end
  
  def add_console(level=nil, stdout=true, stderr=true)
    """Add the ability to log to the console, to our logger"
    if @console
      return
    end

    if stdout
      conlogger = Log4r::StdoutOutputter.new(@name)
      conlogger.formatter = @fmt
    end

    if stderr
      errlogger = Log4r::StderrOutputter.new(@name)
      errlogger.formatter = @fmt
    end
    
    if level then
      conlogger.level = _set_level(level)
    end
    
    @log.add(conlogger)
    @console = true
  end
  
  def debug(msg)
    """Log the message at the debug level"""
    @log.debug(msg)
  end
  
  def info(msg)
    """Log the message at the info level"""
    @log.info(msg)
  end
  
  def warn(msg)
    """Log the message at the warning level"""
    @log.warn(msg)
  end
  
  def error(msg)
    """Log the message at the error level"""
    @log.error(msg.to_s())
  end
  
  def fatal(msg)
    """Log the message at the fatal level"""
    @log.fatal(msg)
  end
  
  def to_s
    """The string representation is just the logger name"""
    return @name
  end
  
end
